DROP DATABASE IF EXISTS progetto_finale;
CREATE DATABASE progetto_finale;
USE progetto_finale; 

CREATE TABLE amministratore (
	amministratoreid INTEGER NOT NULL AUTO_INCREMENT,
	username VARCHAR(150) NOT NULL UNIQUE,
	psw VARCHAR(20) NOT NULL,
	PRIMARY KEY (amministratoreid)
);

CREATE TABLE store (
	storeid INTEGER NOT NULL AUTO_INCREMENT,
	nome VARCHAR(150) NOT NULL UNIQUE,
	codice VARCHAR(20) NOT NULL UNIQUE,
	indirizzo VARCHAR(150) NOT NULL,
	url_immagine VARCHAR(150),
	PRIMARY KEY (storeid)
);

CREATE TABLE utente (
	utenteid INTEGER NOT NULL AUTO_INCREMENT,
	username  VARCHAR(150) NOT NULL UNIQUE,
	psw VARCHAR(20) NOT NULL,
	indirizzo VARCHAR(150) NOT NULL,
	PRIMARY KEY (utenteid)
);

CREATE TABLE prodotto (
	prodottoid INTEGER NOT NULL AUTO_INCREMENT,
	nome VARCHAR(150) NOT NULL,
	codiceProdotto VARCHAR(20) NOT NULL UNIQUE,
	descrizione TEXT NOT NULL,
	prezzo FLOAT NOT NULL,
    quantita INTEGER NOT NULL,
	storerif INTEGER NOT NULL,
    PRIMARY KEY (prodottoid),
	FOREIGN KEY (storerif) REFERENCES store(storeid) ON DELETE CASCADE
);

CREATE TABLE ordine (
	ordineid INTEGER NOT NULL AUTO_INCREMENT,
	codiceOrdine VARCHAR(150) NOT NULL UNIQUE, 
	orario TIMESTAMP NOT NULL,
	utenterif INTEGER NOT NULL,
	PRIMARY KEY (ordineid),
	FOREIGN KEY (utenterif) REFERENCES utente(utenteid) ON DELETE CASCADE
);

CREATE TABLE ordine_prodotto (
	ordine_id INTEGER NOT NULL,
	prodotto_id INTEGER NOT NULL,
	quantita INTEGER NOT NULL DEFAULT 1,
    PRIMARY KEY (ordine_id, prodotto_id),
    FOREIGN KEY (ordine_id) REFERENCES ordine(ordineid) ON DELETE CASCADE,
    FOREIGN KEY (prodotto_id) REFERENCES prodotto(prodottoid) ON DELETE CASCADE
);

insert into amministratore (username,psw) values 
("admin1","123");

insert into store (nome,indirizzo,codice) values
("Mediaworld","Via delle Vie","COD123"),
("Euronics","Strada delle strade","COD321");

insert into utente (username,psw,indirizzo) values
("Francesca","Santi","Asti"),
("Andrea","Caria","Sardegna"),
("Davide","Palmisano","Torino");

insert into prodotto (nome,codiceProdotto,descrizione,quantita,prezzo,storerif) values
("Cuffie","CH654","Cuffie costose",20,230,1),
("Altoparlante","ALT56","Altoparlante costoso",30,330,1),
("Tablet","TAB34","Tablet costose",20,230,1),
("IPhone","IPH24","Telefono costoso",10,250,2);

insert into ordine (codiceOrdine,utenterif,orario) values 
("ORD-1","3","2021-03-17 15:30"),
("ORD-2","1","2021-03-17 20:20");

insert into ordine_prodotto(ordine_id,prodotto_id, quantita) values
(1,1,2),
(1,2,3),
(1,3,1),
(2,4,1),
(2,1,3);

select * from store where codice = "COD123";

select storeid, nome, indirizzo, url_immagine from store ;

select prodottoid, nome, codiceProdotto, descrizione, quantita, prezzo from prodotto;

select prodottoid, nome, codiceProdotto, descrizione, quantita, prezzo from prodotto where storerif = 1;

select ordineid, codiceOrdine, utenterif, orario from ordine;

select ordineid, codiceOrdine, utenterif, orario from ordine;

select * from ordine;

select *
from ordine
join ordine_prodotto on ordine.ordineid = ordine_prodotto.ordine_id
join prodotto on ordine_prodotto.prodotto_id = prodotto.prodottoId;
    
select prodotto.nome, prodotto.prodottoid, prodotto.codiceProdotto, prodotto.prezzo, prodotto.descrizione, prodotto.StoreId, ordine_prodotto.quantita
	from prodotto
	join ordine_prodotto on prodotto.prodottoid = ordine_prodotto.prodotto_id
	join ordine on ordine_prodotto.ordine_id = ordine.ordineid where ordine.ordineid = 1;