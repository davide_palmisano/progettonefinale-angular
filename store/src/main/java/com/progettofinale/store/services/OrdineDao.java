package com.progettofinale.store.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;

import com.progettofinale.store.connession.Connettore;
import com.progettofinale.store.models.Ordine;
import com.progettofinale.store.models.Prodotto;

public class OrdineDao implements Dao<Ordine> {

	@Override
	public Ordine getById(int ID) throws SQLException {
		
		Connection conn = Connettore.getIstanza().getConnessione();
		ProdottoDao proDao = new ProdottoDao();
		UtenteDao uteDao = new UtenteDao();
		String query = "select ordineid, codiceOrdine, orario, utenterif from ordine"
					 + " where ordineid = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1,ID);
		ResultSet risultato = ps.executeQuery();
		risultato.next();
		Ordine temp = new Ordine();
		temp.setOrdineid(risultato.getInt(1)); 
		temp.setCodiceOrdine(risultato.getString(2));
		temp.setOrario(risultato.getTimestamp(3).toLocalDateTime());
		temp.setUtenterif(risultato.getInt(4));  
		temp.setElencoProdotti(proDao.getAllSingleOrder(temp));
		temp.setUser(uteDao.getById(temp.getUtenterif()));
		return temp;
		
	}

	@Override
	public Ordine getByCod(String codice) throws SQLException {
		Connection conn = Connettore.getIstanza().getConnessione();
		UtenteDao uteDao = new UtenteDao();
		ProdottoDao proDao = new ProdottoDao();
		String query = "select ordineid, codiceOrdine, orario, utenterif from ordine"
					 + " where codiceOrdine = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setString(1,codice);
		ResultSet risultato = ps.executeQuery();
		risultato.next();
		Ordine temp = new Ordine();
		temp.setOrdineid(risultato.getInt(1)); 
		temp.setCodiceOrdine(risultato.getString(2)); 
		temp.setOrario(risultato.getTimestamp(3).toLocalDateTime()); 
		temp.setUtenterif(risultato.getInt(4)); 
		temp.setElencoProdotti(proDao.getAllSingleOrder(temp));
		temp.setUser(uteDao.getById(temp.getUtenterif()));
		return temp;
	}

	@Override
	public ArrayList<Ordine> getAll() throws SQLException {
		Connection conn = Connettore.getIstanza().getConnessione();
		ProdottoDao proDao = new ProdottoDao();
		UtenteDao uteDao = new UtenteDao();
		String query= "select ordineid, codiceOrdine, orario, utenterif from ordine";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ArrayList<Ordine> elenco = new ArrayList<Ordine>();
		ResultSet risultato = ps.executeQuery();
		while(risultato.next()) {
			Ordine temp = new Ordine();
			temp.setOrdineid(risultato.getInt(1));
			temp.setCodiceOrdine(risultato.getString(2));
			temp.setOrario(risultato.getTimestamp(3).toLocalDateTime());
			temp.setUtenterif(risultato.getInt(4));
			temp.setElencoProdotti(proDao.getAllSingleOrder(temp));
			temp.setUser(uteDao.getById(temp.getUtenterif()));
			elenco.add(temp);
		}
		return elenco;
	}
	
	public void newOrder(Ordine t, ArrayList<Prodotto> elencoProdotto) throws SQLException {
		this.insert(t);
		Connection conn = Connettore.getIstanza().getConnessione();
		String query = "insert into ordine_prodotto (ordine_id, prodotto_id, quantita) values (?, ?, ?)";

		for(Prodotto iProd: elencoProdotto) {
			PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, t.getOrdineid());
			ps.setInt(2, iProd.getProdottoid());
			ps.setInt(3, iProd.getQuantita());
			ps.executeUpdate();
		}
		
	}
	
		
	public int insertOrd(Ordine t) throws SQLException {
		LocalDateTime ora = LocalDateTime.now();
		Connection conn = Connettore.getIstanza().getConnessione();
		String query= "insert into ordine (codiceOrdine, orario, utenterif) values (?, ?, ?)";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
       	ps.setString(1, t.getCodiceOrdine());
       	ps.setTimestamp(2, Timestamp.valueOf(ora));
       	ps.setInt(3, 1);
       	
       	ps.executeUpdate();
       	ResultSet risultato = ps.getGeneratedKeys();
       	risultato.next();
       	t.setOrdineid(risultato.getInt(1));
		return t.getOrdineid();
		
	}

	@Override
	public boolean delete(Ordine t) throws SQLException {
		Connection conn = Connettore.getIstanza().getConnessione();
		
		String query = "DELETE FROM ordine WHERE codiceProdotto = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, t.getCodiceOrdine());
       	
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
		return false;
		
	}

	@Override
	public boolean update(Ordine t) throws SQLException {
		Connection conn = Connettore.getIstanza().getConnessione();

		String query = "UPDATE ordine SET orario = ? utenterif = ? where codice = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setTimestamp(1, Timestamp.valueOf(t.getOrario()));
       	ps.setInt(2, t.getUtenterif());
       	ps.setString(3, t.getCodiceOrdine());
    	
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
       	return false;
		
	}

}