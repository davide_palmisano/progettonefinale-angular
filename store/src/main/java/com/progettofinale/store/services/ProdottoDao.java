package com.progettofinale.store.services;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.mysql.jdbc.PreparedStatement;
import com.progettofinale.store.connession.Connettore;
import com.progettofinale.store.models.Ordine;
import com.progettofinale.store.models.Prodotto;
import com.progettofinale.store.models.Store;

public class ProdottoDao implements Dao<Prodotto> {
	
	public ArrayList<Prodotto> getAllSingleStore(Store objStore) throws SQLException{
		
		Connection conn = Connettore.getIstanza().getConnessione();
		ArrayList<Prodotto> elenco = new ArrayList<Prodotto>();
		
		String query= "select prodottoid, nome, codiceProdotto, descrizione, quantita, prezzo, storerif from prodotto where storerif = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, objStore.getStoreid());
		ResultSet risultato = ps.executeQuery();
		while(risultato.next()) {
			Prodotto temp = new Prodotto();
			temp.setProdottoid(risultato.getInt(1)); 
			temp.setNome(risultato.getString(2)); 
			temp.setCodiceProdotto(risultato.getString(3)); 
			temp.setDescrizione(risultato.getString(4)); 
			temp.setQuantita(risultato.getInt(5)); 
			temp.setPrezzo(risultato.getFloat(6));
			temp.setStorerif(risultato.getInt(7));
			elenco.add(temp);
		}
		return elenco;
	}
	
	public ArrayList<Prodotto> getAllSingleOrder(Ordine objOrdine) throws SQLException{
		
		Connection conn = Connettore.getIstanza().getConnessione();
		ArrayList<Prodotto> elenco = new ArrayList<Prodotto>();
		
		String query= "select prodotto.prodottoid, prodotto.nome, prodotto.codiceProdotto, prodotto.descrizione, ordine_prodotto.quantita, prodotto.prezzo, prodotto.storerif"
				+ "	from prodotto"
				+ "	join ordine_prodotto on prodotto.prodottoid = ordine_prodotto.prodotto_id"
				+ "	join ordine on ordine_prodotto.ordine_id = ordine.ordineid where ordine.ordineid = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, objOrdine.getOrdineid());
		ResultSet risultato = ps.executeQuery();
		while(risultato.next()) {
			Prodotto temp = new Prodotto();
			temp.setProdottoid(risultato.getInt(1)); 
			temp.setNome(risultato.getString(2)); 
			temp.setCodiceProdotto(risultato.getString(3)); 
			temp.setDescrizione(risultato.getString(4)); 
			temp.setQuantita(risultato.getInt(5)); 
			temp.setPrezzo(risultato.getFloat(6));
			temp.setStorerif(risultato.getInt(7));
			elenco.add(temp);
		}
		
		return elenco;
	}
	
	@Override
	public Prodotto getById(int ID) throws SQLException {
		
		Connection conn = Connettore.getIstanza().getConnessione();
		
		String query= "select prodottoid, nome, codiceProdotto, descrizione, quantita, prezzo, storerif from prodotto where prodottoid = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1,ID);
		ResultSet risultato = ps.executeQuery();
		risultato.next();
		Prodotto temp = new Prodotto();
		temp.setProdottoid(risultato.getInt(1)); 
		temp.setNome(risultato.getString(2)); 
		temp.setCodiceProdotto(risultato.getString(3)); 
		temp.setDescrizione(risultato.getString(4)); 
		temp.setQuantita(risultato.getInt(5)); 
		temp.setPrezzo(risultato.getFloat(6));
		temp.setStorerif(risultato.getInt(7));
		return temp;
		
	}

	@Override
	public Prodotto getByCod(String codice) throws SQLException {

		Connection conn = Connettore.getIstanza().getConnessione();
		
		String query= "select prodottoid, nome, codiceProdotto, descrizione, quantita, prezzo, storerif from prodotto where codiceProdotto = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setString(1,codice);
		ResultSet risultato = ps.executeQuery();
		risultato.next();
		Prodotto temp = new Prodotto();
		temp.setProdottoid(risultato.getInt(1)); 
		temp.setNome(risultato.getString(2)); 
		temp.setCodiceProdotto(risultato.getString(3)); 
		temp.setDescrizione(risultato.getString(4)); 
		temp.setQuantita(risultato.getInt(5)); 
		temp.setPrezzo(risultato.getFloat(6)); 
		temp.setStorerif(risultato.getInt(7));
		return temp;
	}

	@Override
	public ArrayList<Prodotto> getAll() throws SQLException {
		
		Connection conn = Connettore.getIstanza().getConnessione();
		ArrayList<Prodotto> elenco = new ArrayList<Prodotto>();
		
		String query= "select prodottoid, nome, codiceProdotto, descrizione, quantita, prezzo, storerif from prodotto";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ResultSet risultato = ps.executeQuery();
		while(risultato.next()) {
			Prodotto temp = new Prodotto();
			temp.setProdottoid(risultato.getInt(1)); 
			temp.setNome(risultato.getString(2)); 
			temp.setCodiceProdotto(risultato.getString(3)); 
			temp.setDescrizione(risultato.getString(4)); 
			temp.setQuantita(risultato.getInt(5)); 
			temp.setPrezzo(risultato.getFloat(6)); 
			temp.setStorerif(risultato.getInt(7));
			elenco.add(temp);
		}
		return elenco;
	}

	@Override
	public boolean insert(Prodotto t) throws SQLException {
		Connection conn = Connettore.getIstanza().getConnessione();
		String query = "insert into prodotto (nome, codiceProdotto, descrizione, quantita, prezzo, storerif) values (?, ?, ?, ?, ?)";
		
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
       	ps.setString(1, t.getNome());
       	ps.setString(2, t.getCodiceProdotto());
       	ps.setString(3, t.getDescrizione());
       	ps.setInt(4, t.getQuantita());
       	ps.setFloat(5, t.getPrezzo());
       	ps.setInt(6,t.getStorerif());
       	
       	ps.executeUpdate();
       	ResultSet risultato = ps.getGeneratedKeys();
       	risultato.next();
       	t.setProdottoid(risultato.getInt(1));
		
		return true;
	}

	@Override
	public boolean delete(Prodotto t) throws SQLException {
		Connection conn = Connettore.getIstanza().getConnessione();
		
		String query = "DELETE FROM prodotto WHERE codiceProdotto = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, t.getCodiceProdotto());
       	
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
		return false;
	}

	@Override
	public boolean update(Prodotto t) throws SQLException {
		Connection conn = Connettore.getIstanza().getConnessione();

		String query = "UPDATE prodotto SET  nome = ? descrizione = ? quantita = ? prezzo = ? storerif = ? WHERE codiceProdotto = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, t.getNome());
       	ps.setString(2, t.getDescrizione());
       	ps.setInt(3, t.getQuantita());
    	ps.setFloat(4, t.getPrezzo());
    	ps.setInt(5, t.getStorerif());
    	ps.setString(6, t.getCodiceProdotto());

       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
 
       	return false;
		
	}	

}