package com.progettofinale.store.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.progettofinale.store.connession.Connettore;
import com.progettofinale.store.models.Store;

public class StoreDao implements Dao<Store> {

	@Override
	public Store getById(int ID) throws SQLException {
		
		ProdottoDao proDao = new ProdottoDao();
		Connection conn = Connettore.getIstanza().getConnessione();
		
		
		String query= "select storeid, nome, codice, indirizzo, url_immagine from store where storeid = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1,ID);
		ResultSet risultato = ps.executeQuery();
		risultato.next();
		Store temp = new Store();
		temp.setStoreid(risultato.getInt(1)); 
		temp.setNome(risultato.getString(2));
		temp.setCodice(risultato.getString(3));
		temp.setIndirizzo(risultato.getString(4)); 
		temp.setUrl_immagine(risultato.getString(5)); 
		temp.setElencoProdotti(proDao.getAllSingleStore(temp));	
		return temp;
	}

	@Override
	public Store getByCod(String codice) throws SQLException {
		
		ProdottoDao proDao = new ProdottoDao();
		Connection conn = Connettore.getIstanza().getConnessione();
		
		String query= "select storeid, nome, codice, indirizzo, url_immagine from store where codice = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setString(1,codice);
		ResultSet risultato = ps.executeQuery();
		risultato.next();
		Store temp = new Store();
		temp.setStoreid(risultato.getInt(1)); 
		temp.setNome(risultato.getString(2)); 
		temp.setCodice(risultato.getString(3));
		temp.setIndirizzo(risultato.getString(4)); 
		temp.setUrl_immagine(risultato.getString(5)); 
		temp.setElencoProdotti(proDao.getAllSingleStore(temp));	
		return temp;
	}

	@Override
	public ArrayList<Store> getAll() throws SQLException {
		
		ProdottoDao proDao = new ProdottoDao();
		ArrayList<Store> elenco = new ArrayList<Store>();
		Connection conn = Connettore.getIstanza().getConnessione();
		
		String query= "select storeid, nome, codice, indirizzo, url_immagine from store";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ResultSet risultato = ps.executeQuery();
		while(risultato.next()) {
			Store temp = new Store();
			temp.setStoreid(risultato.getInt(1)); 
			temp.setNome(risultato.getString(2)); 
			temp.setCodice(risultato.getString(3));
			temp.setIndirizzo(risultato.getString(4)); 
			temp.setUrl_immagine(risultato.getString(5)); 
			temp.setElencoProdotti(proDao.getAllSingleStore(temp));	
			elenco.add(temp);
		}
		return elenco;
	}

	@Override
	public boolean insert(Store t) throws SQLException {
		Connection conn = Connettore.getIstanza().getConnessione();
		String query = "insert into store (nome, codice, indirizzo, url_immagine) values (?, ?, ?, ?)";
		
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
       	ps.setString(1, t.getNome());
       	ps.setString(2, t.getCodice());
       	ps.setString(3, t.getIndirizzo());
       	ps.setString(4, t.getUrl_immagine());
       	
       	ps.executeUpdate();
       	ResultSet risultato = ps.getGeneratedKeys();
       	risultato.next();
       	
       	t.setStoreid(risultato.getInt(1));
		
		return true;
	}

	@Override
	public boolean delete(Store t) throws SQLException {
		Connection conn = Connettore.getIstanza().getConnessione();
		
		String query = "DELETE FROM store WHERE codice = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, t.getCodice());
       	
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
       	return false;
		
	}

	@Override
	public boolean update(Store t) throws SQLException {
		Connection conn = Connettore.getIstanza().getConnessione();

		String query = "UPDATE store SET nome = ? indirizzo = ? url_immagine = ? WHERE codice = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, t.getNome());
       	ps.setString(2, t.getIndirizzo());
       	ps.setString(3, t.getUrl_immagine());
    	ps.setString(4, t.getCodice());

       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
       	return false;
	}

}