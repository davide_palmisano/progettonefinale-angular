package com.progettofinale.store.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.progettofinale.store.connession.Connettore;
import com.progettofinale.store.models.Utente;

public class UtenteDao implements Dao<Utente> {

	@Override
	public Utente getById(int ID) throws SQLException {
		Connection conn = Connettore.getIstanza().getConnessione();
		String query= "select utenteid, username, psw, indirizzo from utente where utenteid = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1,ID);
		ResultSet risultato = ps.executeQuery();
		risultato.next();
		Utente temp = new Utente();
		temp.setUtenteid(risultato.getInt(1)); 
		temp.setUsername(risultato.getString(2)); 
		temp.setPsw(risultato.getString(3)); 
		temp.setIndirizzo(risultato.getString(4)); 
		
		return temp;
	}

	public Utente getByUser(String username) throws SQLException {
		Connection conn = Connettore.getIstanza().getConnessione();
		String query= "select utenteid, username, psw, indirizzo from utente where username = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setString(1,username);
		ResultSet risultato = ps.executeQuery();
		risultato.next();
		Utente temp = new Utente();
		temp.setUtenteid(risultato.getInt(1)); 
		temp.setUsername(risultato.getString(2)); 
		temp.setPsw(risultato.getString(3)); 
		temp.setIndirizzo(risultato.getString(4)); 
		
		return temp;
	}

	@Override
	public Utente getByCod(String username) throws SQLException {
		Connection conn = Connettore.getIstanza().getConnessione();
		String query= "select utenteid, username, psw, indirizzo from utente where username = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setString(1,username);
		ResultSet risultato = ps.executeQuery();
		risultato.next();
		Utente temp = new Utente();
		temp.setUtenteid(risultato.getInt(1)); 
		temp.setUsername(risultato.getString(2)); 
		temp.setPsw(risultato.getString(3)); 
		temp.setIndirizzo(risultato.getString(4)); 
		
		return temp;
	}

	@Override
	public ArrayList<Utente> getAll() throws SQLException {
		Connection conn = Connettore.getIstanza().getConnessione();
		ArrayList<Utente> elenco = new ArrayList<Utente>();
		String query= "select utenteid, username, psw, indirizzo from utente";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
	
		ResultSet risultato = ps.executeQuery();
		while(risultato.next()) {
			Utente temp = new Utente();
			temp.setUtenteid(risultato.getInt(1)); 
			temp.setUsername(risultato.getString(2)); 
			temp.setPsw(risultato.getString(3)); 
			temp.setIndirizzo(risultato.getString(4)); 
		}
		
		return elenco;
	
	}

	@Override
	public boolean insert(Utente t) throws SQLException {
		Connection conn = Connettore.getIstanza().getConnessione();
		String query = "insert into utente (username, psw, indirizzo) values (?, ?, ?)";
		
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
       	ps.setString(1, t.getUsername());
       	ps.setString(2, t.getPsw());
       	ps.setString(3, t.getIndirizzo());
       	
       	ps.executeUpdate();
       	ResultSet risultato = ps.getGeneratedKeys();
       	risultato.next();
       	t.setUtenteid(risultato.getInt(1));
		
		return true;
		
	}

	@Override
	public boolean delete(Utente t) throws SQLException {
		Connection conn = Connettore.getIstanza().getConnessione();
		
		String query = "delete from utente where username = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, t.getUsername());
       	
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
		return false;
	}

	@Override
	public boolean update(Utente t) throws SQLException {
		Connection conn = Connettore.getIstanza().getConnessione();

		String query = "UPDATE utente SET psw = ? indirizzo = ? WHERE username = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, t.getPsw());
       	ps.setString(2, t.getIndirizzo());
       	ps.setString(3, t.getUsername());
    
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
 
       	return false;
	}

}