import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DettaglioOrdineComponent } from './dettaglio-ordine/dettaglio-ordine.component';
import { ElencoStoreComponent } from './elenco-store/elenco-store.component';
import { InsertComponent } from './insert/insert.component';
import { ListaProdottiComponent } from './lista-prodotti/lista-prodotti.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {path: "utente/insert", component : InsertComponent },
  {path: "utente/login", component : LoginComponent },
  {path: "utente/listaProdotti/:nome/:codice", component : ListaProdottiComponent },
  {path: "utente/dettaglioOrdine/:id/:totale", component : DettaglioOrdineComponent },
  {path: "utente/elencoStore/:user", component : ElencoStoreComponent },
  {path: "", redirectTo: "utente/login",  pathMatch: "full"}  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
