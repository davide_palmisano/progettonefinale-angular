import { Prodotto } from "./prodotto";

export class Ordine {
    ordineid : number | undefined;
    utenterif : number | undefined;
    elencoProdotti : any = [] ;
    codiceOrdine : String | undefined;
    orario : String | undefined;
    Utente : any | undefined;
    Store : any | undefined;
}
