import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { logging } from 'selenium-webdriver';
import { Utente } from './utente';
import { Ordine } from './ordine';

const endpoint = "http://localhost:8081/" ;

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient) { 

  }

  inserisciUtente(obj_utente :Utente){
    let header = new HttpHeaders();
    header = header.set('Content-Type','application/json');
    return this.http.post(endpoint+"inserisciutente",JSON.stringify(obj_utente),{headers: header} )   
  }

  verificaLogin(var_user : String ,var_psw : String){   
    // Funzione che verifica il log in , invio un array di due stringhe
    let header = new HttpHeaders();
    let obj_login = 
    {
      username : var_user,
      psw : var_psw
    } ; 
    header = header.set('Content-Type','application/json');
    return this.http.post(endpoint+"verificalogin",JSON.stringify(obj_login),{headers: header} )    
  }

  getAllStore(){
    return this.http.get(endpoint+"store");
  }
  getAllProdotti(var_codice : string){
    return this.http.get(endpoint+"store/"+var_codice);
  }

  getProdottoById(var_id : number){
    return this.http.get(endpoint+"prodotto/"+var_id) ;
  }
  getOrdineById(var_id : number){
    return this.http.get(endpoint+"ordine/"+var_id) ;
  }

  insertOrdine(obj_ordine : any){
    let header = new HttpHeaders();
    header = header.set('Content-Type','application/json');
    return this.http.post(endpoint+"inserisciordine",JSON.stringify(obj_ordine),{headers: header} );
  }

}
