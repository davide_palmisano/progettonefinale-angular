import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestService } from '../rest.service';
import { Utente } from '../utente';

@Component({
  selector: 'app-insert',
  templateUrl: './insert.component.html',
  styleUrls: ['./insert.component.css']
})
export class InsertComponent implements OnInit {
  inputUser : String = new String ;
  inputPsw : String = new String ;
  inputIndirizzo : String = new String ;

  
  constructor(private rest : RestService, private router : Router) { }
  
  ngOnInit(): void {
  }
  
  effettuaIscrizione(){
    let temp : Utente = new Utente ; 
    temp.username = this.inputUser;
    temp.psw = this.inputPsw;
    temp.indirizzo = this.inputIndirizzo;
    console.log(temp);
    this.rest.inserisciUtente(temp).subscribe(
      (responso)=> {
        if(responso == true){
          this.router.navigateByUrl("utente/elencoStore/"+temp.username);
          alert("Utente inserito con successo")
        }
        else {
          alert("Errore nell'inserimento !")
        }
      },
      (errore)=> {
        this.router.navigateByUrl("utente/elencoStore");

      }
    );
  }
}
