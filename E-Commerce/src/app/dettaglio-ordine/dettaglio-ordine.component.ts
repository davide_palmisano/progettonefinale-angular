import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Ordine } from '../ordine';
import { RestService } from '../rest.service';

@Component({
  selector: 'app-dettaglio-ordine',
  templateUrl: './dettaglio-ordine.component.html',
  styleUrls: ['./dettaglio-ordine.component.css']
})
export class DettaglioOrdineComponent implements OnInit {

  ordine : Ordine = new Ordine ;
  elenco : any = [];
  somma : number = 0 ; 

  constructor(private rest: RestService  ,private act_router : ActivatedRoute, private router : Router) { }

  ngOnInit(): void {
    this.act_router.params.subscribe(
      (ris) => {
        let var_id = ris.id ;
        this.somma = ris.totale ;
        this.rest.getOrdineById(var_id).subscribe(
          (ris : any) => {
             this.ordine.elencoProdotti = ris.elencoProdotti;
            console.log(ris);
          }
        );
      }
    ) ; 
  }

  inserisciOrdine(){
    let obj_ordine;
    this.rest.insertOrdine(obj_ordine).subscribe(
      (ris) => {
        
          alert("Ordine effettuato con successo");
        
      },
       

      (errore) => {

      }
    );
  }
}
