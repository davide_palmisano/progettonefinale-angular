import { templateJitUrl } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Ordine } from '../ordine';
import { Prodotto } from '../prodotto';
import { RestService } from '../rest.service';

@Component({
  selector: 'app-lista-prodotti',
  templateUrl: './lista-prodotti.component.html',
  styleUrls: ['./lista-prodotti.component.css']
})
export class ListaProdottiComponent implements OnInit {

  nome_store : String = "" ;
  elenco_prod : any = [] ;
  var_ordine : Ordine = new Ordine  ;
  somma : number = 0; 
  contatore : number = 0 ; 
  inputQuantita : any = [] ;

  constructor(private rest: RestService  ,private act_router : ActivatedRoute, private router : Router) {
    
    this.act_router.params.subscribe(
      (ris) => {
        this.nome_store = ris.nome ;
        console.log(this.nome_store);
      }
    );
      
    // Chiamata per avere elenco di prodotti
    let var_codice : string = "" ; 
     this.act_router.params.subscribe(
       (ris) => {
         var_codice = ris.codice ; 
       }
     );
    this.rest.getAllProdotti(var_codice).subscribe(
      (risultato : any) => {
        this.elenco_prod = risultato.elencoProdotti; 
        console.log(this.elenco_prod);
      }
    );
   }


  ngOnInit(): void {
 

  }
  addCarrello(obj_button : any){
    
    this.contatore ++ ;
    let var_id : number = obj_button.id;
    let var_idx : number = obj_button.name;
  
    // Funzione che mi cerca il prodotto per id , poi lo assegno ad una variabile
    this.rest.getProdottoById(var_id).subscribe(
      (ris ) => {
        console.log(ris);
        let prodotto = ris ; 
        this.var_ordine.elencoProdotti.push(prodotto) ;
        this.dettaglioCarrello(var_idx);
      },
      (errore) => {
        console.log("Richiesta non andata a buon fine");
      }
      
    );

    // Generato l'oggetto lo aggiungo al carrello
    
    

    
  }
  
  generaOrdine(){
  
  }
  
  dettaglioCarrello(idx : number){
    let qta = this.inputQuantita[idx] ; 
    console.log(qta);
    let lista_prod = this.var_ordine.elencoProdotti ; 
    console.log(lista_prod);
    for (let i = 0; i < lista_prod.length; i++) {
      let prod_temp  = lista_prod[i];
      this.somma += prod_temp.prezzo*qta ;      
    }
  
  }

  navigaDettaglio(){
    this.rest.insertOrdine(this.var_ordine).subscribe(
      (ris) => {
        let id = ris ;
        console.log(id) ;
        this.router.navigateByUrl("utente/dettaglioOrdine/"+id+"/"+this.somma);
      }
    );
    console.log(this.var_ordine);
  }
}
