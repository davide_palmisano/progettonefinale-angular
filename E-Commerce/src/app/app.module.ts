import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InsertComponent } from './insert/insert.component';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule, HttpHandler } from '@angular/common/http';
import { ElencoStoreComponent } from './elenco-store/elenco-store.component';
import { ListaProdottiComponent } from './lista-prodotti/lista-prodotti.component';
import { DettaglioOrdineComponent } from './dettaglio-ordine/dettaglio-ordine.component';

@NgModule({
  declarations: [
    AppComponent,
    InsertComponent,
    LoginComponent,
    ElencoStoreComponent,
    ListaProdottiComponent,
    DettaglioOrdineComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
