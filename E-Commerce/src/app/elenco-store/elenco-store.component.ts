import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { RestService } from '../rest.service';

@Component({
  selector: 'app-elenco-store',
  templateUrl: './elenco-store.component.html',
  styleUrls: ['./elenco-store.component.css']
})
export class ElencoStoreComponent implements OnInit {

  inputUser : String = "";
  elenco_store : any = [] ;
 
  constructor(private rest: RestService, private act_router : ActivatedRoute, private router : Router) { 

    this.act_router.params.subscribe(
          (ris) => {
            this.inputUser = ris.user ;
            console.log(this.inputUser);
          }
        );

            // Chiamata per avere elenco di store
    this.rest.getAllStore().subscribe(
      (risultato) => {
        console.log(risultato);
        this.elenco_store = risultato ; 
      }
    );
  }

  ngOnInit(): void {}

  paginaNegozio(obj_button : any){
    let nome : String = obj_button.id ;
    let codice : String = obj_button.name ;
    console.log(codice) ;
    this.router.navigateByUrl("utente/listaProdotti/"+nome+"/"+codice);
  }
}
