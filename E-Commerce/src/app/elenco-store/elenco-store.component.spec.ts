import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ElencoStoreComponent } from './elenco-store.component';

describe('ElencoStoreComponent', () => {
  let component: ElencoStoreComponent;
  let fixture: ComponentFixture<ElencoStoreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ElencoStoreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ElencoStoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
