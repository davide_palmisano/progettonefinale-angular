import { templateJitUrl } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestService } from '../rest.service';
import { Utente } from '../utente';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  inputUser : String = new String ;
  inputPsw : String = new String ;
  
  constructor(private rest : RestService, private router : Router) {
    
  }

  ngOnInit(): void {
  }

  effettuaLogin(){
    let var_user = this.inputUser ; 
    let var_psw = this.inputPsw ; 
    console.log(var_user);
    console.log(var_psw);
    this.rest.verificaLogin(var_user,var_psw).subscribe(
      (risposta) => {
        if (risposta == true){
          this.router.navigateByUrl("utente/elencoStore/"+var_user);
        }
        else{
          alert("Username o Password errati!")
        }
      },
      (errore) => {
        // Provvisorio fino a che non collego tutto
        console.log(var_psw);
        
        this.router.navigateByUrl("utente/elencoStore/"+var_user);
      }
    ) ;
  }
}
