export class Prodotto {
    nome : String | undefined;
    codice : String | undefined;
    descrizione : String | undefined;
    prezzo : number | undefined;
    qta : number | undefined;
}
